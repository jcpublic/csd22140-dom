
// 1. Get the button element
let b = document.querySelector("button");

// 2. Add the event handler
// MEANING: When person clicks on button, run the sayHello() function
b.addEventListener("click", changeBackground);

function changeBackground() {
    let color = document.querySelector("input").value;
    document.querySelector("body").style.backgroundColor = color;    
}

