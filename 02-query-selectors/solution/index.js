// 1. Create your variables
let name = document.querySelector(".student-1");
let school = document.querySelector("h3");
let bestGrade = document.querySelector(".grade-2");

// 2. Modify the content of those elements
name.innerText = "Rachida";
school.innerText = "Lambton College";
bestGrade.innerText = "A+";

// 3. EXERCISE: Change all instances of "Ferris Bueller" to "Rachida"

// OPTION 1: EASY: Doing it in 2 lines
// OPTION 2: EASY+: Doing it in 1 line
document.querySelector(".student-2").innerText = "Rachida";








