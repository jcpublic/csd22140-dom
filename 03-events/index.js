
// 1. Get the button element
let b = document.querySelector("button");

// 2. Add the event handler
// MEANING: When person clicks on button, run the sayHello() function
b.addEventListener("click", changeBackground);

// 3. Write the code that should run in response to the click
function sayHello() {
    console.log("HELLO WORLD");
}
// EXERCISE:  Update your code so that when person clicks on button, background changes to BLUE
function changeBackground() {
    // 1. get the body tag
    let body = document.querySelector("body");
    // 2. write the JS to change background color
    body.style.backgroundColor = "blue";
}

// EXERCISE 2: Update code so when you press the RED button, bakground changes to red
let b2 = document.querySelector("#redButton");
b2.addEventListener("click", changeRed);
function changeRed() {
    let body = document.querySelector("body");
    body.style.backgroundColor = "red";
}



