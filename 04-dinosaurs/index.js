// EXERCISE: Write the code to do this
// 1. When person clicks on button, change the "dinosaurs are cool" text to match whatever the person typed in the textbox.
// 2. Clear the text box (remove any text in the textbox)
// -------------------------

// 1. Get the button
let button = document.querySelector("button.change-text");
// 2. Add an event listener to the button
button.addEventListener("click", changeText);

// 3. Inside your event listener function:
function changeText() {
    console.log("HELLO WORLD!");
    //      -   Get the text from the textbox
    let textbox = document.querySelector("input");
    let userInput = textbox.value; // .value is the property to get text out of textbox
    console.log("User typed: " + userInput);

    //      -   Get the "Dinosaurs are cool" element
    //      -   Change .innerText to be the text from textbox
    document.querySelector("h1").innerText = userInput;

    //      -   Clear the textbox
    textbox.value = "";
}