// VARIABLES
let interestBox = document.querySelector("#tbInterestRate");
let loanAmountBox = document.querySelector("#tbLoanAmount");
let minPaymentBox = document.querySelector("#tbMinPayment");

// CLICK HANDLER
let button = document.querySelector("button");
button.addEventListener("click", calculatePayments);

function calculatePayments() {
    let loanAmount = loanAmountBox.value;
    let minPayment = minPaymentBox.value;
    let interest = interestBox.value;
    console.log("Loan Amount: " + loanAmount);
    console.log("Interest rate: " + interest);
    console.log("Min payment: " + minPayment);

    // calculate formula
    let A = loanAmount;
    let P = minPayment;
    let i = interest / 100;
    let numerator = -1*Math.log(1-i*A/P);
    let denominator = Math.log(1+i)

    let n = numerator / denominator;
    console.log("It will take you " + n + " months to pay off");

    // @TODO: HOMEWORK:
    // Output result to the screen: "It will take you 5 months to pay off."
    // "5 months" should be BOLD and UNDERLINED
    // if n > 12, change "__ months" to say "___ years", where ___ is the number of years it will take to pay off.
}